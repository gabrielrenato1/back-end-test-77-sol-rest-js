import './Result.css';
import '../../App.css'
import React from 'react';

class Result extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
  }

  render() {
    return (
      <div className="App">
        <div>
          <h1>Resultado</h1>
        </div>
        <div className="">
          <textarea className="input" name="name" rows="15"></textarea>
        </div>
      </div>
    );
  }
}

export default Result;
