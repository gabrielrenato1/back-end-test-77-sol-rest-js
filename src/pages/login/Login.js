import './Login.css';
import '../../App.css'
import React from 'react';
import axios from 'axios';
import {browserHistory} from 'react-router';

class Login extends React.Component {

  constructor(props){
    super(props);
    console.log(localStorage.getItem('token'));
  }

  async handleSubmit(event) {

    event.preventDefault();
    var response = await axios.post('http://127.0.0.1:8000/api/login?email='+event.target.login.value+'&password='+event.target.pass.value,
        ).then(response => {
            localStorage.setItem('token', response.data.token);
            console.log(localStorage.getItem('token'));
            return response;
        })
        .catch(error => {
            console.log(error);
        }
    )

    if (response.status == 200) {
      browserHistory.push('/simulador');
    }
    
  }

  render() {
    return (
      <div className="App-login">
        <div className="circle"></div>
        <div className="">
          <form onSubmit={this.handleSubmit} method="post">
            <div className="">
              <input className="input" type="text" name="login" placeholder="E-mail"/>
            </div>
            <div className="">
              <input className="input" type="password" name="pass" placeholder="Senha"/>
            </div>
            <div className="">
              <button className="button" type="submit" name="button">Login</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
