import './Simulator.css';
import '../../App.css'
import React from 'react';
import axios from 'axios';
import {browserHistory} from 'react-router';


class Simulator extends React.Component {

  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    if (localStorage.getItem("token") == null) {
      browserHistory.push('/login');
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    await axios.get('http://127.0.0.1:8000/api/simulator?cep='+event.target.cep.value+'&account_value='+event.target.account_value.value+'&type='+event.target.type.value+'&token='+localStorage.getItem('token'),
        ).then(response => {
          this.setState({
            parcelamento: response.data.parcelamento,
            integradores: response.data.integradores_regiao,
            economia: response.data.economia,
            potencial: response.data.potencial,
            loading: false,
          })
        })
        .catch(error => {
            console.log(error);
        }
    )
  }

  render() {
    return(
      <div className="App-simulator">
        <div>
          <h1>Simulador</h1>
        </div>

        <div className="content-simulator">
          <form onSubmit={this.handleSubmit} method="get">
            <div className="">
              <input className="input" type="text" name="cep" placeholder="CEP"/>
            </div>
            <div className="">
              <input className="input" type="text" name="account_value" placeholder="Valor da conta"/>
            </div>
            <div className="">
              <input className="input" type="text" name="type" placeholder="Tipo de telhado"/>
            </div>

            <div className="">
              <button className="button" type="submit" name="button">Simular</button>
            </div>
          </form>
        </div>
        <div>
          <div>
            <h1>Resultado</h1>
          </div>
          <div className="result">
            <div className="">
              {this?.state == null ? '' :
              this.state.parcelamento.map((item, i) => (
                                <div className="flex">
                                  <div className="result-attribute">Parcelas: {item.parcelas}</div>
                                  <div className="result-attribute">Taxa minima: {parseFloat(item.taxa_minina).toFixed(2)}</div>
                                  <div className="result-attribute">Taxa maxima: {parseFloat(item.taxa_maxima).toFixed(3)}</div>
                                  <div className="result-attribute">Valor minimo {parseFloat(item.valor_minimo).toFixed(3)}</div>
                                  <div className="result-attribute">Valor Máximo: {parseFloat(item.valor_minimo).toFixed(3)}</div>
                                </div>
                            ))}
            </div>
            <div className="result-attribute">
              {this?.state == null ? '' : 'Empresas parceiras: ' + this.state.integradores}
            </div>
            <div className="result-attribute">
              {this?.state == null ? '' : 'Economia: ' + this.state.economia}
            </div>
            <div className="result-attribute">
              {this?.state == null ? '' : 'Potencial: ' + this.state.potencial}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Simulator;
