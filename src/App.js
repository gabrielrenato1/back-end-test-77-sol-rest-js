import Login from './pages/login/Login';
import Simulator from './pages/simulator/Simulator';

import {Router, Route, browserHistory} from 'react-router';

function App() {
  return (
    <Router history={browserHistory}>
      <Route path={"login"} component={Login}/>
      <Route path={"simulador"} component={Simulator}/>
    </Router>
  );
}

export default App;
